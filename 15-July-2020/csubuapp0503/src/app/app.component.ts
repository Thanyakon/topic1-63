import { Component, OnInit } from '@angular/core';

interface Lecturer {
  name: string;
  image: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'csubuapp0503';
    baseurl = 'http://www.math.sci.ubu.ac.th/assets/staffimages/';

  constructor() { }
  lecturers: Lecturer[] = [
      { image: 'http://picsum.photos/id/1001/84/82/', name: 'Dr.K' },
      { image: 'http://picsum.photos/id/1002/84/82/', name: 'Assist.Prof.Chayaporn' },
      { image: 'http://picsum.photos/id/1003/84/82/', name: 'Dr.Tossaporn' },
      { image: 'http://picsum.photos/id/1004/84/82/', name: 'Dr.Phaichayon' },
      { image: 'http://picsum.photos/id/1005/84/82/', name: 'Dr.Woody' },
      { image: 'http://picsum.photos/id/1006/84/82/', name: 'Aj.Wasana' },
      { image: 'http://picsum.photos/id/1007/84/82/', name: 'Dr.Paul' },
      { image: 'http://picsum.photos/id/1008/84/82/', name: 'Dr.Suphawadee' },
      { image: 'http://picsum.photos/id/1009/84/82/', name: 'Aj.Wayo' }
  ];
}
