
import { Student } from '../models/student';


export const STUDENT: Student[] = [
    { id: 60111111, name: 'Chris hemsworth', email: 'chris.h@gmail.com'},
    { id: 60111112, name: 'Jack reacher', email: 'jack.r@gmail.com'},
    { id: 60111113, name: 'Emma Charlotte', email: 'emam.c@gmail.com'},
    { id: 60111114, name: 'Angelina Jolie', email: 'Angelina.j@gmail.com'},
    { id: 60111115, name: 'Scarlett Ingrid', email: 'Scarlett.i@gmail.com'},
];
