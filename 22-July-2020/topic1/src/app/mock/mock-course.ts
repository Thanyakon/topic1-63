
import { Course } from '../models/course';

export const COURSE: Course[] = [
  { title: 'Computational Mathematics', description: 'Systems of linear equations and numerical solutions; matrices; determinants; numerical solutions of equations in one variable; linear transformations; interpolation and extrapolation; numerical differentiation and integration; eigenvalues and eigenvectors'},
  { title: 'Software Engineering', description: 'Introduction to software engineering; software development life cycle; software process modeling; requirement engineering; software analysis and design; software development; software testing; project management; software maintenance'},
  { title: 'Data Communications and Internet Technologies', description: 'Data'},
  { title: 'Selected Topic in Computer Science I', description: 'หัวข้อทางวิทยาการคอมพิวเตอร์ที่เกี่ยวข้องกับเทคโนโลยีในปัจจุบัน โดยเนื้อหาในรายวิชานี้มุ่งเน้นให้นักศึกษามีความสามารถพัฒนาแอพพลิเคชันต่างๆ ด้วยเทคโนโลยี node.js'},

];
