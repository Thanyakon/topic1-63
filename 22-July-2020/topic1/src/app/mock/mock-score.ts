import { Score } from '../models/score';


import { STUDENT } from './mock-stuent';
import { Student } from '../models/student';


const student: Student[] = STUDENT;

export const SCORE: Score[] = [
  { student: student[0], score: 10, full_mack: 10},
  { student: student[1], score: 20, full_mack: 20},
  { student: student[2], score: 30, full_mack: 30},
  { student: student[3], score: 40, full_mack: 40},
  { student: student[4], score: 50, full_mack: 50},
];

