import { Lecturer } from '../models/lecturer';


export const LECTURER: Lecturer[] = [
  {id: 11231232, name: 'Elon musk ( อีลอน มัสก์ )'},
  {id: 23243667, name: 'Mark Zuckerberg ( มาร์ก ซักเคอร์เบิร์ก )'},
  {id: 33324354, name: 'Bill gates ( บิล เกตส์ )'},
  {id: 44647532, name: 'Jack ma ( แจ็ค หม่า )'},
  {id: 55467893, name: 'Jeff bezos ( เจฟฟ์ เบโซส์ )'},

];
