import { Component  } from '@angular/core';
import { STUDENT } from './mock/mock-stuent';
import { Student } from './models/student';
import { Lecturer } from './models/lecturer';
import { LECTURER } from './mock/mock-lecturer';
import { Course } from './models/course';
import { COURSE } from './mock/mock-course';
import { SCORE } from './mock/mock-score';
import { Score } from './models/score';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Coures Web Application';
  description = '';

  student: Student[] = STUDENT;
  lecturer: Lecturer[] = LECTURER;
  coure: Course[] = COURSE;
  score: Score[] = SCORE;

  // student = [
  //   { id: 60111111, name: 'Chris hemsworth', email: 'tom@gmail.com'},
  //   { id: 60111112, name: 'Jack reacher', email: 'tom@gmail.com'},
  //   { id: 60111114, name: 'Tom hank', email: 'tom@gmail.com'},
  //   { id: 60111115, name: 'Tom hank', email: 'tom@gmail.com'},
  //   { id: 60111116, name: 'Tom hank', email: 'tom@gmail.com'},
  // ];
    tom =  { id: 60111116, name: 'Tom hank', email: 'tom@gmail.com'};

}
