import { Identifiers } from '@angular/compiler';

export interface Score{
  student: Identifiers;
  score: number;
  full_mack: number;
}
