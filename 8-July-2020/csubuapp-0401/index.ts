import * as readline from 'readline'

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

rl.question('Enter name : ', (answer) => {
  console.log(`Hello : ${answer}`)
  rl.close()
})