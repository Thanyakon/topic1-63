import { Hero } from '../models/hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Dr Nice' , hp: 100 },
  { id: 12, name: 'Narco' , hp: 100 },
  { id: 13, name: 'Bombasto' , hp: 100 },
  { id: 14, name: 'Celeritas' , hp: 100 },
  { id: 15, name: 'Magneta' , hp: 100 },
  { id: 16, name: 'RubberMan' , hp: 100 },
  { id: 17, name: 'Dynama' , hp: 100 },
  { id: 18, name: 'Dr IQ' , hp: 100 },
  { id: 19, name: 'Magma' , hp: 100 },
  { id: 20, name: 'Tornado' , hp: 100 }
];
