import { Component, OnInit } from '@angular/core';
import { Hero } from '../models/hero';
import { HEROES } from '../mocks/mock-heroes';
import { HeroService } from '../service/hero.service';
import { MessageService } from '../service/message.service';



@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {

  hero: Hero = {
    id: 1,
    name: 'Batman',
    hp: 100
  };

  selectedHero: Hero;

  heroes = HEROES;


  heros: Hero[];

  constructor(private heroService: HeroService , private messageService: MessageService  ) { }

  // tslint:disable-next-line: typedef
  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes().subscribe(
      (heros: Hero[]) => {
        this.heros = heros;
      },
      (err: any) => {
        this.messageService.add("err");
      });
    this.selectedHero = this.heros[0];
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
    // console.log(this.selectedHero);

    // tslint:disable-next-line: no-console
    // console.info(this.selectedHero);
    this.messageService.add(`use user ${hero.name} - ${hero.id} `);


  }



}
