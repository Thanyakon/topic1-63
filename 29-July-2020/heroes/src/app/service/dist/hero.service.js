"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.HeroService = void 0;
var core_1 = require("@angular/core");
var mock_heroes_1 = require("../mocks/mock-heroes");
var rxjs_1 = require("rxjs");
var HeroService = /** @class */ (function () {
    function HeroService(messageService, http) {
        this.messageService = messageService;
        this.http = http;
        this.heroesUrl = 'api/heroes'; // URL to web api
    }
    HeroService.prototype.log = function (message) {
        this.messageService.add("HeroService: " + message);
    };
    HeroService.prototype.handleError = function (operation, result) {
        var _this = this;
        if (operation === void 0) { operation = 'operation'; }
        return function (error) {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // TODO: better job of transforming error for user consumption
            _this.log(operation + " failed: " + error.message);
            // Let the app keep running by returning an empty result.
            return rxjs_1.of(result);
        };
    };
    HeroService.prototype.getHeroes = function () {
        this.messageService.add('HeroService: fetched heroes');
        // return this.http.get<Hero[]>(this.heroesUrl);
        return rxjs_1.of(mock_heroes_1.HEROES);
    };
    HeroService.prototype.getHero = function (id) {
        // TODO: send the message _after_ fetching the hero
        this.messageService.add("HeroService: fetched hero id=" + id);
        return rxjs_1.of(mock_heroes_1.HEROES.find(function (hero) { return hero.id === id; }));
    };
    HeroService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], HeroService);
    return HeroService;
}());
exports.HeroService = HeroService;
